package org.pirola.scalajs.matrix

import java.time.Instant

import scala.language.implicitConversions
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom._
import org.scalajs.dom

import scala.collection.mutable.{ArrayBuffer => MutableArray}
import scala.util.Random

@JSExport
object Matrix {

  @JSExport
  def start(canvas: html.Canvas) = {
    println("Start matrix class")

    val ctx: CanvasRenderingContext2D = canvas
      .getContext("2d")
      .asInstanceOf[dom.CanvasRenderingContext2D]

    canvas.height = window.innerHeight
    canvas.width = window.innerWidth
    val dropSize = 12
    val columns  = canvas.width / dropSize

    val chinese: Array[String] =
      """ムタ二コク1234567890シモラキリエスハヌトユABCDEF""".split("")

    val drops = MutableArray.range(0, columns)
    for (i <- 0 until drops.length) drops(i) = 0

    val fps      = 20
    var lastNow  = Instant.now().toEpochMilli
    val interval = 1000 / fps

    val fontStyle = s"${dropSize}px arial"

    def draw: Unit = {
      val now   = Instant.now().toEpochMilli
      val delta = now - lastNow

      if (delta > interval) {
        lastNow = now - (delta % interval)

        ctx.fillStyle = "rgba(0, 0, 0, 0.05)"
        ctx.fillRect(0, 0, canvas.width, canvas.height)

        ctx.fillStyle = "#0f0"
        //ctx.fillStyle = s"#${Random.nextInt(4095).toHexString}"
        ctx.font = fontStyle

        for (i <- 0 until drops.length) {

          val text = chinese(Random.nextInt(chinese.length))
          ctx.fillText(text, i * dropSize, drops(i) * dropSize)

          if (drops(i) * dropSize > canvas.height && Math.random() > 0.975)
            drops(i) = 0

          drops(i) = drops(i) + 1
        }
      }
      window.requestAnimationFrame((d: Double) => draw)
    }

    draw
  }
}

import sbt.Keys._

val commonSettings = Seq(
  scalaVersion := "2.11.8",
  name := """scala-js-matrix""",
  version := "1.0"
)

lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(ScalaJSPlugin)
  .settings(
    libraryDependencies ++= Dependencies.library ++ Seq(
      "org.scala-js" %%% "scalajs-dom"       % "0.9.0",
      "com.lihaoyi"  %%% "scalarx"           % "0.3.2",
      "org.scala-js" %%% "scalajs-java-time" % "0.2.0"),
    resolvers ++= Dependencies.resolvers,
    scalacOptions ++= Seq("-feature", "-language:_", "-unchecked", "-deprecation")
  )

onLoad in Global := (Command.process("scalafmt", _: State)) compose (onLoad in Global).value

# scala-js-matrix
See Matrix code... and Follow the White Rabbit ([demo](https://pirola.org/demo/scala-js-matrix.html))

## Build
* `sbt ~fastOptJS` - for development
* `sbt fullOptJS` - for production
* `target/scala-2.11/classes/` - folder for output HTML files

## License
GNU Version 3
